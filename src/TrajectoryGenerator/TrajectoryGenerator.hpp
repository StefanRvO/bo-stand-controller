#pragma once
#include <limits.h>
#include <stdint.h>

#include "as5046/as5046.hpp"
#include "common/CPULoadTimer/CPULoadTimer.hpp"
#include <array>

struct Waypoint {
  Waypoint() {}

  explicit Waypoint(uint16_t pos) : pos(pos) {}
  uint16_t pos      = 0xFFFF;
  int64_t  endTime  = 0;
  bool     active   = false;
  bool     wasHit   = false;
  bool     inserted = false;
};

template <uint8_t MAX_WAYPOINTS>
struct TrajectoryProgress {
  TrajectoryProgress(const as5046::as5046& encoder, float gearing) : encoder(encoder), gearing(gearing) {}
  uint8_t                             currentWaypoint = 0xFF;
  int64_t                             startTime       = 0;
  uint16_t                            startPosition   = 0;
  bool                                speedLatched    = false;
  float                               latchedSpeed    = 0;
  std::array<Waypoint, MAX_WAYPOINTS> waypoints;
  uint16_t                            distance = 0;
  const as5046::as5046&               encoder;
  const float                         gearing;
};

template <uint8_t MAX_WAYPOINTS>
class Trajectory {
 public:
  Trajectory(
    const as5046::as5046& m1_encoder,
    const as5046::as5046& m2_encoder,
    CPULoadTimer&         timer,
    float                 gearing_m1,
    float                 gearing_m2)
    : m1_progress(m1_encoder, gearing_m1),
      m2_progress(m2_encoder, gearing_m2),
      timer(timer)
  {
  }
  ~Trajectory() = default;

  float normDistance(float p1, float p2) { return sqrtf(p1 * p1 + p2 * p2); }

  void setWaypoint(std::array<Waypoint, 2> w, uint8_t index)
  {
    if(index < m1_progress.waypoints.size())
    {
      m1_progress.waypoints[index].pos = w[0].pos;
    }
    if(index < m2_progress.waypoints.size())
    {
      m2_progress.waypoints[index].pos = w[1].pos;
    }
  }
  std::array<Waypoint, 2> getWaypoint(uint8_t index)
  {
    if(index < m1_progress.waypoints.size() && index < m2_progress.waypoints.size())
    {
      return {m1_progress.waypoints[index], m2_progress.waypoints[index]};
    }
  }

  uint64_t getTime() const { return timer.getFullTimer(); }

  uint8_t getCurrentM1Waypoint() { return m1_progress.currentWaypoint; }
  uint8_t getCurrentM2Waypoint() { return m2_progress.currentWaypoint; }

  uint32_t getCRC()
  {
    uint32_t crc = 0;
    crcEngine.compute_partial_start(&crc);
    for(uint8_t i = 0; i < MAX_WAYPOINTS; i++)
    {
      if(m1_progress.waypoints[i].pos == 0xFFFF || m2_progress.waypoints[i].pos == 0xFFFF)
      {
        break;
      }
      crcEngine.compute_partial(&m1_progress.waypoints[i].pos, sizeof(m1_progress.waypoints[i].pos), &crc);
      crcEngine.compute_partial(&m2_progress.waypoints[i].pos, sizeof(m2_progress.waypoints[i].pos), &crc);
    }
    crcEngine.compute_partial_stop(&crc);
    return crc;
  }

  void finalize(uint8_t _waypoint_count, uint64_t time_ms)
  {
    if(!isReset)
    {
      return;
    }
    isReset        = false;
    waypoint_count = _waypoint_count;
    finalize_waypoints(m1_progress);
    finalize_waypoints(m2_progress);
    moveSpeed = getMoveSpeed(time_ms * 0.001f);
    if(waypoint_count > 1)
    {
      connectToTrajectory();
    }

    finalize_time(m1_progress, m2_progress);
  }

  void connectToTrajectory()
  {
    float    min_distance    = std::numeric_limits<float>::max();
    uint16_t closestPoint[2] = {0, 0};

    const uint16_t m1_position = m1_progress.encoder.getPosition();
    const uint16_t m2_position = m2_progress.encoder.getPosition();

    const auto& m1_waypoints = m1_progress.waypoints;
    const auto& m2_waypoints = m2_progress.waypoints;

    uint8_t replacedWaypoint = 0;

    for(uint8_t i = 1; i < waypoint_count; i++)
    {
      // Get m1 and m2 distance between start and end point in line
      const float m1_line_distance = m1_progress.encoder.getDistance(m1_waypoints[i - 1].pos, m1_waypoints[i].pos);
      const float m2_line_distance = m2_progress.encoder.getDistance(m2_waypoints[i - 1].pos, m2_waypoints[i].pos);

      const float max_distance = std::max(2.f, std::max(std::abs(m1_line_distance), std::abs(m2_line_distance)));
      const float stepSize     = 1.f / max_distance;
      float       waypoint_min_distance = 0xFFFFFFFF;

      for(float point = 0.0f; point <= 1.f; point += stepSize)
      {
        const uint16_t sample_point_m1 = uint16_t(m1_waypoints[i - 1].pos + point * m1_line_distance) & 0xFFF;
        const uint16_t sample_point_m2 = uint16_t(m2_waypoints[i - 1].pos + point * m2_line_distance) & 0xFFF;

        const float distance = normDistance(
          float(std::abs(m1_progress.encoder.getDistance(sample_point_m1, m1_position))) * m1_progress.gearing,

          float(std::abs(m2_progress.encoder.getDistance(sample_point_m2, m2_position))) * m2_progress.gearing);

        if(distance < waypoint_min_distance)
        {
          waypoint_min_distance = distance;
        }
        if(distance < min_distance)
        {
          min_distance     = distance;
          closestPoint[0]  = sample_point_m1;
          closestPoint[1]  = sample_point_m2;
          replacedWaypoint = i - 1;
        }
      }

      const float endpointDistance = normDistance(
        float(std::abs(m1_progress.encoder.getDistance(m1_waypoints[i].pos, m1_position))) * m1_progress.gearing,

        float(std::abs(m2_progress.encoder.getDistance(m2_waypoints[i].pos, m2_position))) * m2_progress.gearing);

      if(waypoint_min_distance == min_distance && endpointDistance < (m1_progress.gearing + m2_progress.gearing) * 3)
      {
        min_distance     = endpointDistance;
        closestPoint[0]  = m1_waypoints[i].pos;
        closestPoint[1]  = m2_waypoints[i].pos;
        replacedWaypoint = i;
      }
    }

    // Overwrite the found waypoint with the new position
    m1_progress.waypoints[replacedWaypoint].pos      = closestPoint[0];
    m2_progress.waypoints[replacedWaypoint].pos      = closestPoint[1];
    m1_progress.waypoints[replacedWaypoint].inserted = true;
    m2_progress.waypoints[replacedWaypoint].inserted = true;
    m1_progress.currentWaypoint                      = 0;
    m2_progress.currentWaypoint                      = 0;

    waypoint_count -= replacedWaypoint;
    // Move current waypoint so we start going to the found one
    for(uint8_t i = 0; i < replacedWaypoint; i++)
    {
      for(uint8_t j = 1; j < m1_progress.waypoints.size(); j++)
      {
        m1_progress.waypoints[j - 1] = m1_progress.waypoints[j];
        m2_progress.waypoints[j - 1] = m2_progress.waypoints[j];
      }
    }
  }

  uint32_t getTrajectoryDistance()
  {
    uint32_t total_distance   = 0;
    uint16_t last_waypoint[2] = {m1_progress.startPosition, m2_progress.startPosition};
    if(waypoint_count > 1)
    {
      last_waypoint[0] = m1_progress.waypoints[0].pos;
      last_waypoint[1] = m2_progress.waypoints[0].pos;
    }
    for(uint8_t i = 0; i < MAX_WAYPOINTS; i++)
    {
      if(!m1_progress.waypoints[i].active)
      {
        break;
      }
      total_distance += std::max(
        uint32_t(std::abs(m1_progress.encoder.getDistance(last_waypoint[0], m1_progress.waypoints[i].pos)))
          * m1_progress.gearing,

        uint32_t(std::abs(m2_progress.encoder.getDistance(last_waypoint[1], m2_progress.waypoints[i].pos)))
          * m2_progress.gearing);
      last_waypoint[0] = m1_progress.waypoints[i].pos;
      last_waypoint[1] = m2_progress.waypoints[i].pos;
    }
    return total_distance;
  }

  float getMoveSpeed(float seconds) { return float(getTrajectoryDistance()) / seconds; }

  void reset()
  {
    for(auto& waypoint : m1_progress.waypoints)
    {
      waypoint = Waypoint();
    }
    for(auto& waypoint : m2_progress.waypoints)
    {
      waypoint = Waypoint();
    }
    m1_progress.currentWaypoint = 0xFF;
    m2_progress.currentWaypoint = 0xFF;
    waypoint_count              = 0;
    isReset                     = true;
  }

  uint8_t getWaypointCount()
  {
    for(uint8_t i = 0; i < MAX_WAYPOINTS; i++)
    {
      if(m1_progress.waypoints[i].pos == 0xFFFF || m2_progress.waypoints[i].pos == 0xFFFF)
      {
        return i;
      }
    }
    return MAX_WAYPOINTS;
  }

  void finalize_waypoints(TrajectoryProgress<MAX_WAYPOINTS>& progress)
  {
    for(auto& waypoint : progress.waypoints)
    {
      waypoint.active = false;
      waypoint.wasHit = false;
    }

    if(waypoint_count > progress.waypoints.size() - 1)
    {
      return;
    }

    progress.startPosition   = progress.encoder.getPosition();
    progress.currentWaypoint = 0;

    for(uint8_t i = 0; i < waypoint_count; i++)
    {
      progress.waypoints[i].active = true;
    }
  }

  void finalize_time(TrajectoryProgress<MAX_WAYPOINTS>& progress_1, TrajectoryProgress<MAX_WAYPOINTS>& progress_2)
  {
    progress_1.startTime = getTime();
    progress_2.startTime = getTime();

    uint16_t last_waypoint[2] = {progress_1.startPosition, progress_2.startPosition};

    int64_t waypoint_start_time = progress_1.startTime;

    for(uint8_t i = 0; i < MAX_WAYPOINTS; i++)
    {
      if(!progress_1.waypoints[i].active)
      {
        continue;
      }
      uint32_t waypoint_distance = std::max(
        uint32_t(std::abs(progress_1.encoder.getDistance(last_waypoint[0], progress_1.waypoints[i].pos)))
          * progress_1.gearing,

        uint32_t(std::abs(progress_2.encoder.getDistance(last_waypoint[1], progress_2.waypoints[i].pos)))
          * progress_2.gearing);
      last_waypoint[0] = progress_1.waypoints[i].pos;
      last_waypoint[1] = progress_2.waypoints[i].pos;

      if(!progress_1.waypoints[i].inserted)
      {
        waypoint_start_time = waypoint_start_time + timer.fromSeconds(waypoint_distance / moveSpeed);
      }
      else
      {
        waypoint_start_time =
          waypoint_start_time + timer.fromSeconds((waypoint_distance * INSERTED_WAYPOINT_SPEED_DIVIDER) / moveSpeed);
      }
      progress_1.waypoints[i].endTime = waypoint_start_time;
      progress_2.waypoints[i].endTime = waypoint_start_time;
    }
  };

  uint16_t getTargetM1()
  {
    const auto& w = m1_progress.waypoints[m1_progress.currentWaypoint];
    if(!w.active)
    {
      return 0;
    }
    return w.pos;
  }

  uint16_t getTargetM2()
  {
    const auto& w = m2_progress.waypoints[m2_progress.currentWaypoint];
    if(!w.active)
    {
      return 0;
    }
    return w.pos;
  }

  float getNeededSpeedM1() { return getNeededSpeed(m1_progress, m2_progress); }
  float getNeededSpeedM2() { return getNeededSpeed(m2_progress, m1_progress); }

  float getNeededSpeed(TrajectoryProgress<MAX_WAYPOINTS>& progress, TrajectoryProgress<MAX_WAYPOINTS>& other_progress)
  {
    if(progress.currentWaypoint >= progress.waypoints.size())
    {
      return nanf("");
    }
    auto* currentWaypoint = &progress.waypoints[progress.currentWaypoint];

    if(!currentWaypoint->active)
    {
      return nanf("");
    }

    const int64_t currentTime          = getTime();
    uint16_t      waypointDistanceLeft = 0;

    while(true)
    {
      waypointDistanceLeft = std::abs(progress.encoder.getDistance(progress.encoder.getPosition(), currentWaypoint->pos));

      if(waypointDistanceLeft <= WAYPOINT_HIT_TOLERANCE)
      {
        currentWaypoint->wasHit = true;
      }

      const int64_t timeToWaypoint = currentWaypoint->endTime - progress.startTime;
      const auto&   otherWaypoint  = other_progress.waypoints[progress.currentWaypoint];

      // Move to next waypoint if a lot over time or we have hit the waypoint and is within 200 ms of the expected end time
      if(
        currentTime - currentWaypoint->endTime > timeToWaypoint
        || (currentWaypoint->wasHit && otherWaypoint.wasHit && timer.toSeconds(currentTime - currentWaypoint->endTime) > -0.2f))
      {
        progress.speedLatched = false;
        progress.currentWaypoint++;
        currentWaypoint = &progress.waypoints[progress.currentWaypoint];
        if(!currentWaypoint->active)
        {
          return nanf("");
        }
        continue;
      }
      break;
    }

    if(currentWaypoint->wasHit)
    {
      return 0;
    }

    const auto& nextWaypoint = progress.waypoints[progress.currentWaypoint + 1];

    float speed = float(waypointDistanceLeft) / timer.toSeconds(currentWaypoint->endTime - currentTime);
    if(currentTime >= currentWaypoint->endTime)
    {
      return 1000;
    }

    if(timer.toSeconds(currentTime - progress.startTime) < WAYPOINT_SPEED_RAMP_TIME_S * 0.001f)
    {
      // Start of movement, smooth the start a bit
      speed = speed * MIN_SPEED_RAMP
              + (timer.toSeconds((currentTime - progress.startTime)) / WAYPOINT_SPEED_RAMP_TIME_S) * speed
                  * (1 - MIN_SPEED_RAMP);
    }
    else if(timer.toSeconds(currentWaypoint->endTime - currentTime) < WAYPOINT_SPEED_RAMP_TIME_S)
    {
      if(!progress.speedLatched)
      {
        progress.latchedSpeed = speed;
        progress.speedLatched = true;
      }
      // Ramp into next waypoint (or stop if this is the last one)
      if(nextWaypoint.active)
      {
        const float nextWaypointSpeed = std::abs(progress.encoder.getDistance(currentWaypoint->pos, nextWaypoint.pos))
                                        / timer.toSeconds(nextWaypoint.endTime - currentTime);
        float speed_minimal = progress.latchedSpeed * MIN_SPEED_RAMP;
        float smoothProgress =
          timer.toSeconds(currentWaypoint->endTime - currentTime) / float(WAYPOINT_SPEED_RAMP_TIME_S);
        float speed_current_waypoint_part = smoothProgress * progress.latchedSpeed * (1.f - MIN_SPEED_RAMP);
        float speed_next_waypoint_part    = (1 - smoothProgress) * nextWaypointSpeed * (1.f - MIN_SPEED_RAMP);

        speed = speed_minimal + speed_current_waypoint_part + speed_next_waypoint_part;
      }
      else
      {
        speed = progress.latchedSpeed * MIN_SPEED_RAMP
                + (timer.toSeconds(currentWaypoint->endTime - currentTime) / float(WAYPOINT_SPEED_RAMP_TIME_S))
                    * progress.latchedSpeed * (1 - MIN_SPEED_RAMP);
      }
    }
    return speed;
  }

 public:
  TrajectoryProgress<MAX_WAYPOINTS>              m1_progress;
  TrajectoryProgress<MAX_WAYPOINTS>              m2_progress;
  static constexpr float                         WAYPOINT_SPEED_RAMP_TIME_S = 0.250f;
  static constexpr uint64_t                      WAYPOINT_HIT_TOLERANCE     = 2;
  static constexpr float                         MIN_SPEED_RAMP             = 1. / 3.;
  MbedCRC<POLY_32BIT_ANSI, 32, CrcMode::BITWISE> crcEngine;
  uint8_t                                        waypoint_count = 0;
  float                                          moveSpeed;
  bool                                           isReset = true;
  const CPULoadTimer&                            timer;
  static constexpr float                         INSERTED_WAYPOINT_SPEED_DIVIDER = 2.5f;
};