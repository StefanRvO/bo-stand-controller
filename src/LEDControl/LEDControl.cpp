#include "LEDControl.hpp"

namespace ledcontrol
{
LEDControl::LEDControl(PinName pin) : led(pin)
{
  blinkPatterns[0] = 0xF1FF0000F1FF0000;
}

void LEDControl::service(uint32_t state)
{
  const auto pattern = blinkPatterns[state];

  intervalCounter = (intervalCounter + 1) % STEP_INTERVAL;
  if(intervalCounter % STEP_INTERVAL == 0)
  {
    stepCounter = (stepCounter + 1) % (sizeof(pattern) * 8);
    led         = (~pattern >> stepCounter) & 1;
  }
}

}  // namespace ledcontrol