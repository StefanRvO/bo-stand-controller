#pragma once
#include "common/bo_pins.h"
#include "mbed.h"

// P6
#define P6_PIN6         P0_15  // PROG
#define P6_PIN7         P0_16  // CHIPSELECT
#define P6_PIN9         P0_7   // SCL
#define P6_PIN10        P0_6   // SDA
#define ENC1_SDA        P6_PIN10
#define ENC1_SCL        P6_PIN9
#define ENC1_CHIPSELECT P6_PIN7
#define ENC1_PROG       P6_PIN6

// P7
#define P7_PIN6         P1_22  // PROG
#define P7_PIN7         P1_25  // CHIPSELECT
#define P7_PIN9         P2_2   // SCL
#define P7_PIN10        P2_1   // SDA
#define ENC2_SDA        P7_PIN10
#define ENC2_SCL        P7_PIN9
#define ENC2_CHIPSELECT P7_PIN7
#define ENC2_PROG       P7_PIN6

// IC600 (Motor 1 IC)
#define M1_FG        P1_18  // Throgh 10K
#define M1_VSP       P1_20  // Through some kind of PWM-to analog circuit i think
#define M1_DIRECTION P0_30
#define M1_RESET     P0_29

// IC800 (Motor 2 IC)
#define M2_FG        P1_26  //(Through 10K)
#define M2_VSP       P1_23  // Through some kind of PWM-to analog circuit i think
#define M2_DIRECTION P0_18
#define M2_RESET     P0_22
