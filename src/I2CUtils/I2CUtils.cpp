#include "I2CUtils.hpp"

#include <stdio.h>

namespace I2CUtils
{

void scanI2C(I2C& bus, int id)
{
  for(int address = 1; address < 127; address++)
  {
    if(0 == bus.write(address, "11", 1))
    {
      printf("I2C %d, Found at address %3x\n", id, address);
    }
    thread_sleep_for(1);
  }
}

void scanI2C(SoftI2C& bus, int id)
{
  for(int address = 1; address < 127; address++)
  {
    if(0 == bus.write(address, "11", 1))
    {
      printf("I2C %d, Found at address %3x\n", id, address);
    }
    thread_sleep_for(1);
  }
}

}  // namespace I2CUtils