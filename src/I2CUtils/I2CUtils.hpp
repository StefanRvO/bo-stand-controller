#pragma once
#include "SoftI2C/SoftI2C.h"
#include "mbed.h"

namespace I2CUtils
{

void scanI2C(I2C& bus, int id);
void scanI2C(SoftI2C& bus, int id);

}  // namespace I2CUtils