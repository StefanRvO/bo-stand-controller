#pragma once
#include "LPC17xx.h"
#include "mbed.h"

class PulseCounter {
 public:
  enum class CountEvent {
    PCLK           = 0,
    RISING         = 1,
    FALLING        = 2,
    RISING_FALLING = 3,
  };

  enum class CaptureInput {
    CAP_0,
    CAP_1,
  };

  PulseCounter(LPC_TIM_TypeDef* timer, PinName pin, CountEvent event, uint8_t capturePin);
  ~PulseCounter() = default;

  uint32_t getCount();

 private:
  LPC_TIM_TypeDef* timer;
};