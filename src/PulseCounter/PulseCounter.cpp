#include "PulseCounter.hpp"

PulseCounter::PulseCounter(LPC_TIM_TypeDef* timer, PinName pin, CountEvent event, uint8_t capturePin) : timer(timer)
{
  if(pin != NC)
  {
    pin_function(pin, 3);  // Set pinfunction 3 = capture
  }

  timer->TCR = 2;           // Disable and reset timer
  timer->PR  = 0xFFFFFFFF;  // Use full prescaler

  // Set count source
  timer->CTCR &= (~3U);
  timer->CTCR |= static_cast<uint32_t>(event);

  timer->TCR = 1;  // Enable counter
}

uint32_t PulseCounter::getCount()
{
  return timer->PC;
}
