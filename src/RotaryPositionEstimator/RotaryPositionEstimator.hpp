#pragma once
#include <math.h>
#include <stdint.h>

#include "common/Filters/MovingAverage.hpp"
#include <complex>

template <uint8_t BITS>
class RotaryPositionEstimator {
 public:
  RotaryPositionEstimator()  = default;
  ~RotaryPositionEstimator() = default;

  bool update(uint16_t newPosition)
  {
    if(newPosition == lastRawPosition)
    {
      return false;
    }
    positionAverage.update(std::polar(1.0f, newPosition * 2 * F_M_PI * (1.f / TICKS)));
    lastRawPosition = newPosition;
    return true;
  }
  uint16_t getLastRawPosition() { return lastRawPosition; }
  float    getLastPosition() { return std::arg(positionAverage.getLast()) + F_M_PI; }

  static float getAngleDifference(float theta1, float theta2)
  {
    float diff = theta1 - theta2;
    if(diff >= M_PI)
    {
      diff -= 2 * M_PI;
    }
    else if(diff <= -M_PI)
    {
      diff += 2 * M_PI;
    }
    return diff;
  }

  static float getTickDifference(float theta1, float theta2)
  {
    return getAngleDifference(theta1, theta2) * (0.5f / F_M_PI) * TICKS;
  }

 protected:
  static constexpr uint16_t              TICKS  = (1 << BITS);
  static constexpr float                 F_M_PI = M_PI;
  uint16_t                               maxPosition;
  uint16_t                               lastRawPosition = 0;
  MovingAverageF<std::complex<float>, 2> positionAverage;
};