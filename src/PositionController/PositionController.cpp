#include "PositionController.hpp"

#include <algorithm>

PositionController::PositionController(tb6585::tb6585& motor, as5046::as5046& encoder, MotorFeedbackSpeed& feedback)
  : motor(motor),
    encoder(encoder),
    speedFilter(0.0025),
    speedPID(1.f / 250.f, 0, 1 / 30.f),
    feedback(feedback)
{
}

void PositionController::update()
{
  const uint16_t position = encoder.getPosition();
  const int16_t  distance = encoder.getDistance(position, targetFunction());

  direction       = distance > 0 ? Direction::FORWARD : Direction::REVERSE;
  lastNeededSpeed = speedFunction();
  if(!isfinite(lastNeededSpeed))
  {
    active = false;
  }
  if(!active)
  {
    lastNeededSpeed = 0;
  }

  if(direction == Direction::REVERSE)
  {
    lastNeededSpeed *= -1;
  }

  lastNeededSpeed = std::min(lastNeededSpeed, 1000.f);
  lastNeededSpeed = std::max(lastNeededSpeed, -1000.f);

  lastPIDOutput = speedPID.getOutput(feedback.getSpeed(), lastNeededSpeed);
  float speed   = lastPIDOutput + motor.getCurrentSpeed();
  if(speed > 1)
  {
    speed = 1;
  }
  if(speed < -1)
  {
    speed = -1;
  }
  speedFilter.update(speed);

  if(active)
  {
    motor.setSpeed(speedFilter.output);
  }
  else
  {
    motor.setSpeed(0);
  }
  motor.update();
}