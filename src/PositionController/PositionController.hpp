#pragma once

#include "MiniPID/MiniPID.h"
#include "MotorFeedbackSpeed/MotorFeedbackSpeed.hpp"
#include "TrajectoryGenerator/TrajectoryGenerator.hpp"
#include "as5046/as5046.hpp"
#include "common/Filters/EWMA.hpp"
#include "mbed.h"
#include "tb6585/tb6585.hpp"
#include <algorithm>

class PositionController {
 public:
  enum class Direction {
    FORWARD = 0,
    REVERSE = 1,
  };

  PositionController(tb6585::tb6585& motor, as5046::as5046& encoder, MotorFeedbackSpeed& feedback);
  ~PositionController() = default;

  uint16_t getTarget() { return targetFunction(); }
  void     update();
  bool     isActive() { return active; }
  void     stop() { active = false; }
  void     setTrajectoryFunctions(std::function<float(void)> speedF, std::function<uint16_t(void)> targetF)
  {
    active         = true;
    speedFunction  = speedF;
    targetFunction = targetF;
  }

  float getLastNeededSpeed() { return lastNeededSpeed; }

  tb6585::tb6585&               motor;
  as5046::as5046&               encoder;
  Direction                     direction       = Direction::FORWARD;
  bool                          active          = false;
  float                         lastNeededSpeed = 0;
  EWMA                          speedFilter;
  MiniPID                       speedPID;
  MotorFeedbackSpeed&           feedback;
  float                         lastPIDOutput;
  std::function<float(void)>    speedFunction;
  std::function<uint16_t(void)> targetFunction;
};