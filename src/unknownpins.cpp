#include "unknownpins.hpp"

#include "mbed.h"

#if 0

typedef struct {
  DigitalInOut pin;
  uint8_t      val;
} pins_t;

static pins_t unKnownPins[] = {
  // P0
  // {.pin = DigitalInOut(P0_0, PIN_INPUT, PullNone, 0), .val = 1},
  // {.pin = DigitalInOut(P0_1, PIN_INPUT, PullNone, 0), .val = 2},
  {.pin = DigitalInOut(P0_2, PIN_INPUT, PullNone, 0), .val = 3},
  {.pin = DigitalInOut(P0_3, PIN_INPUT, PullNone, 0), .val = 4},
  {.pin = DigitalInOut(P0_4, PIN_INPUT, PullNone, 0), .val = 5},
  {.pin = DigitalInOut(P0_5, PIN_INPUT, PullNone, 0), .val = 6},
  // {.pin = DigitalInOut(P0_6, PIN_INPUT, PullNone, 0), .val = 7},
  // {.pin = DigitalInOut(P0_7, PIN_INPUT, PullNone, 0), .val = 8},
  {.pin = DigitalInOut(P0_8, PIN_INPUT, PullNone, 0), .val = 9},
  {.pin = DigitalInOut(P0_9, PIN_INPUT, PullNone, 0), .val = 10},
  {.pin = DigitalInOut(P0_10, PIN_INPUT, PullNone, 0), .val = 11},
  {.pin = DigitalInOut(P0_11, PIN_INPUT, PullNone, 0), .val = 12},
  {.pin = DigitalInOut(P0_12, PIN_INPUT, PullNone, 0), .val = 13},
  {.pin = DigitalInOut(P0_13, PIN_INPUT, PullNone, 0), .val = 14},
  {.pin = DigitalInOut(P0_14, PIN_INPUT, PullNone, 0), .val = 15},
  // {.pin = DigitalInOut(P0_15, PIN_INPUT, PullNone, 0), .val = 16},
  // {.pin = DigitalInOut(P0_16, PIN_INPUT, PullNone, 0), .val = 17},
  {.pin = DigitalInOut(P0_17, PIN_INPUT, PullNone, 0), .val = 18},
  // {.pin = DigitalInOut(P0_18, PIN_INPUT, PullNone, 0), .val = 19},
  {.pin = DigitalInOut(P0_19, PIN_INPUT, PullNone, 0), .val = 20},
  {.pin = DigitalInOut(P0_20, PIN_INPUT, PullNone, 0), .val = 21},
  {.pin = DigitalInOut(P0_21, PIN_INPUT, PullNone, 0), .val = 22},
  // {.pin = DigitalInOut(P0_22, PIN_INPUT, PullNone, 0), .val = 23},
  {.pin = DigitalInOut(P0_23, PIN_INPUT, PullNone, 0), .val = 24},
  {.pin = DigitalInOut(P0_24, PIN_INPUT, PullNone, 0), .val = 25},
  {.pin = DigitalInOut(P0_25, PIN_INPUT, PullNone, 0), .val = 26},
  {.pin = DigitalInOut(P0_26, PIN_INPUT, PullNone, 0), .val = 27},
  {.pin = DigitalInOut(P0_27, PIN_INPUT, PullNone, 0), .val = 28},
  {.pin = DigitalInOut(P0_28, PIN_INPUT, PullNone, 0), .val = 29},
  // {.pin = DigitalInOut(P0_29, PIN_INPUT, PullNone, 0), .val = 30},
  // {.pin = DigitalInOut(P0_30, PIN_INPUT, PullNone, 0), .val = 31},
  {.pin = DigitalInOut(P0_31, PIN_INPUT, PullNone, 0), .val = 32},
  // P1
  {.pin = DigitalInOut(P1_0, PIN_INPUT, PullNone, 0), .val = 1 + 32 * 1},
  {.pin = DigitalInOut(P1_1, PIN_INPUT, PullNone, 0), .val = 2 + 32 * 1},
  {.pin = DigitalInOut(P1_2, PIN_INPUT, PullNone, 0), .val = 3 + 32 * 1},
  {.pin = DigitalInOut(P1_3, PIN_INPUT, PullNone, 0), .val = 4 + 32 * 1},
  {.pin = DigitalInOut(P1_4, PIN_INPUT, PullNone, 0), .val = 5 + 32 * 1},
  {.pin = DigitalInOut(P1_5, PIN_INPUT, PullNone, 0), .val = 6 + 32 * 1},
  {.pin = DigitalInOut(P1_6, PIN_INPUT, PullNone, 0), .val = 7 + 32 * 1},
  {.pin = DigitalInOut(P1_7, PIN_INPUT, PullNone, 0), .val = 8 + 32 * 1},
  {.pin = DigitalInOut(P1_8, PIN_INPUT, PullNone, 0), .val = 9 + 32 * 1},
  // {.pin = DigitalInOut(P1_9, PIN_INPUT, PullNone, 0), .val = 10 + 32 * 1},
  {.pin = DigitalInOut(P1_10, PIN_INPUT, PullNone, 0), .val = 11 + 32 * 1},
  {.pin = DigitalInOut(P1_11, PIN_INPUT, PullNone, 0), .val = 12 + 32 * 1},
  {.pin = DigitalInOut(P1_12, PIN_INPUT, PullNone, 0), .val = 13 + 32 * 1},
  {.pin = DigitalInOut(P1_13, PIN_INPUT, PullNone, 0), .val = 14 + 32 * 1},
  {.pin = DigitalInOut(P1_14, PIN_INPUT, PullNone, 0), .val = 15 + 32 * 1},
  {.pin = DigitalInOut(P1_15, PIN_INPUT, PullNone, 0), .val = 16 + 32 * 1},
  {.pin = DigitalInOut(P1_16, PIN_INPUT, PullNone, 0), .val = 17 + 32 * 1},
  {.pin = DigitalInOut(P1_17, PIN_INPUT, PullNone, 0), .val = 18 + 32 * 1},
  // {.pin = DigitalInOut(P1_18, PIN_INPUT, PullNone, 0), .val = 19 + 32 * 1},
  {.pin = DigitalInOut(P1_19, PIN_INPUT, PullNone, 0), .val = 20 + 32 * 1},
  // {.pin = DigitalInOut(P1_20, PIN_INPUT, PullNone, 0), .val = 21 + 32 * 1},
  {.pin = DigitalInOut(P1_21, PIN_INPUT, PullNone, 0), .val = 22 + 32 * 1},
  // {.pin = DigitalInOut(P1_22, PIN_INPUT, PullNone, 0), .val = 23 + 32 * 1},
  // {.pin = DigitalInOut(P1_23, PIN_INPUT, PullNone, 0), .val = 24 + 32 * 1},
  {.pin = DigitalInOut(P1_24, PIN_INPUT, PullNone, 0), .val = 25 + 32 * 1},
  // {.pin = DigitalInOut(P1_25, PIN_INPUT, PullNone, 0), .val = 26 + 32 * 1},
  // {.pin = DigitalInOut(P1_26, PIN_INPUT, PullNone, 0), .val = 27 + 32 * 1},
  {.pin = DigitalInOut(P1_27, PIN_INPUT, PullNone, 0), .val = 28 + 32 * 1},
  {.pin = DigitalInOut(P1_28, PIN_INPUT, PullNone, 0), .val = 29 + 32 * 1},
  {.pin = DigitalInOut(P1_29, PIN_INPUT, PullNone, 0), .val = 30 + 32 * 1},
  {.pin = DigitalInOut(P1_30, PIN_INPUT, PullNone, 0), .val = 31 + 32 * 1},
  {.pin = DigitalInOut(P1_31, PIN_INPUT, PullNone, 0), .val = 32 + 32 * 1},
  // P2
  // {.pin = DigitalInOut(P2_0, PIN_INPUT, PullNone, 0), .val = 1 + 32 * 2},
  // {.pin = DigitalInOut(P2_1, PIN_INPUT, PullNone, 0), .val = 2 + 32 * 2},
  // {.pin = DigitalInOut(P2_2, PIN_INPUT, PullNone, 0), .val = 3 + 32 * 2},
  {.pin = DigitalInOut(P2_3, PIN_INPUT, PullNone, 0), .val = 4 + 32 * 2},
  {.pin = DigitalInOut(P2_4, PIN_INPUT, PullNone, 0), .val = 5 + 32 * 2},
  {.pin = DigitalInOut(P2_5, PIN_INPUT, PullNone, 0), .val = 6 + 32 * 2},
  {.pin = DigitalInOut(P2_6, PIN_INPUT, PullNone, 0), .val = 7 + 32 * 2},
  {.pin = DigitalInOut(P2_7, PIN_INPUT, PullNone, 0), .val = 8 + 32 * 2},
  // {.pin = DigitalInOut(P2_8, PIN_INPUT, PullNone, 0), .val = 9 + 32 * 2},
  // {.pin = DigitalInOut(P2_9, PIN_INPUT, PullNone, 0), .val = 10 + 32 * 2},
  {.pin = DigitalInOut(P2_10, PIN_INPUT, PullNone, 0), .val = 11 + 32 * 2},
  {.pin = DigitalInOut(P2_11, PIN_INPUT, PullNone, 0), .val = 12 + 32 * 2},
  {.pin = DigitalInOut(P2_12, PIN_INPUT, PullNone, 0), .val = 13 + 32 * 2},
  {.pin = DigitalInOut(P2_13, PIN_INPUT, PullNone, 0), .val = 14 + 32 * 2},
  {.pin = DigitalInOut(P2_14, PIN_INPUT, PullNone, 0), .val = 15 + 32 * 2},
  {.pin = DigitalInOut(P2_15, PIN_INPUT, PullNone, 0), .val = 16 + 32 * 2},
  {.pin = DigitalInOut(P2_16, PIN_INPUT, PullNone, 0), .val = 17 + 32 * 2},
  {.pin = DigitalInOut(P2_17, PIN_INPUT, PullNone, 0), .val = 18 + 32 * 2},
  {.pin = DigitalInOut(P2_18, PIN_INPUT, PullNone, 0), .val = 19 + 32 * 2},
  {.pin = DigitalInOut(P2_19, PIN_INPUT, PullNone, 0), .val = 20 + 32 * 2},
  {.pin = DigitalInOut(P2_20, PIN_INPUT, PullNone, 0), .val = 21 + 32 * 2},
  {.pin = DigitalInOut(P2_21, PIN_INPUT, PullNone, 0), .val = 22 + 32 * 2},
  {.pin = DigitalInOut(P2_22, PIN_INPUT, PullNone, 0), .val = 23 + 32 * 2},
  {.pin = DigitalInOut(P2_23, PIN_INPUT, PullNone, 0), .val = 24 + 32 * 2},
  {.pin = DigitalInOut(P2_24, PIN_INPUT, PullNone, 0), .val = 25 + 32 * 2},
  {.pin = DigitalInOut(P2_25, PIN_INPUT, PullNone, 0), .val = 26 + 32 * 2},
  {.pin = DigitalInOut(P2_26, PIN_INPUT, PullNone, 0), .val = 27 + 32 * 2},
  {.pin = DigitalInOut(P2_27, PIN_INPUT, PullNone, 0), .val = 28 + 32 * 2},
  {.pin = DigitalInOut(P2_28, PIN_INPUT, PullNone, 0), .val = 29 + 32 * 2},
  {.pin = DigitalInOut(P2_29, PIN_INPUT, PullNone, 0), .val = 30 + 32 * 2},
  {.pin = DigitalInOut(P2_30, PIN_INPUT, PullNone, 0), .val = 31 + 32 * 2},
  {.pin = DigitalInOut(P2_31, PIN_INPUT, PullNone, 0), .val = 32 + 32 * 2},
  // P3
  {.pin = DigitalInOut(P3_0, PIN_INPUT, PullNone, 1 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_1, PIN_INPUT, PullNone, 2 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_2, PIN_INPUT, PullNone, 3 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_3, PIN_INPUT, PullNone, 4 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_4, PIN_INPUT, PullNone, 5 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_5, PIN_INPUT, PullNone, 6 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_6, PIN_INPUT, PullNone, 7 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_7, PIN_INPUT, PullNone, 8 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_8, PIN_INPUT, PullNone, 9 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_9, PIN_INPUT, PullNone, 10 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_10, PIN_INPUT, PullNone, 11 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_11, PIN_INPUT, PullNone, 12 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_12, PIN_INPUT, PullNone, 13 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_13, PIN_INPUT, PullNone, 14 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_14, PIN_INPUT, PullNone, 15 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_15, PIN_INPUT, PullNone, 16 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_16, PIN_INPUT, PullNone, 17 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_17, PIN_INPUT, PullNone, 18 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_18, PIN_INPUT, PullNone, 19 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_19, PIN_INPUT, PullNone, 20 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_20, PIN_INPUT, PullNone, 21 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_21, PIN_INPUT, PullNone, 22 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_22, PIN_INPUT, PullNone, 23 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_23, PIN_INPUT, PullNone, 24 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_24, PIN_INPUT, PullNone, 25 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_25, PIN_INPUT, PullNone, 26 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_26, PIN_INPUT, PullNone, 27 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_27, PIN_INPUT, PullNone, 28 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_28, PIN_INPUT, PullNone, 29 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_29, PIN_INPUT, PullNone, 30 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_30, PIN_INPUT, PullNone, 31 + 32 * 3), .val = 0},
  {.pin = DigitalInOut(P3_31, PIN_INPUT, PullNone, 32 + 32 * 3), .val = 0},
  // P4
  {.pin = DigitalInOut(P4_0, PIN_INPUT, PullNone, 0), .val = 1 + 32 * 4},
  {.pin = DigitalInOut(P4_1, PIN_INPUT, PullNone, 0), .val = 2 + 32 * 4},
  {.pin = DigitalInOut(P4_2, PIN_INPUT, PullNone, 0), .val = 3 + 32 * 4},
  {.pin = DigitalInOut(P4_3, PIN_INPUT, PullNone, 0), .val = 4 + 32 * 4},
  {.pin = DigitalInOut(P4_4, PIN_INPUT, PullNone, 0), .val = 5 + 32 * 4},
  {.pin = DigitalInOut(P4_5, PIN_INPUT, PullNone, 0), .val = 6 + 32 * 4},
  {.pin = DigitalInOut(P4_6, PIN_INPUT, PullNone, 0), .val = 7 + 32 * 4},
  {.pin = DigitalInOut(P4_7, PIN_INPUT, PullNone, 0), .val = 8 + 32 * 4},
  {.pin = DigitalInOut(P4_8, PIN_INPUT, PullNone, 0), .val = 9 + 32 * 4},
  {.pin = DigitalInOut(P4_9, PIN_INPUT, PullNone, 0), .val = 10 + 32 * 4},
  {.pin = DigitalInOut(P4_10, PIN_INPUT, PullNone, 0), .val = 11 + 32 * 4},
  {.pin = DigitalInOut(P4_11, PIN_INPUT, PullNone, 0), .val = 12 + 32 * 4},
  {.pin = DigitalInOut(P4_12, PIN_INPUT, PullNone, 0), .val = 13 + 32 * 4},
  {.pin = DigitalInOut(P4_13, PIN_INPUT, PullNone, 0), .val = 14 + 32 * 4},
  {.pin = DigitalInOut(P4_14, PIN_INPUT, PullNone, 0), .val = 15 + 32 * 4},
  {.pin = DigitalInOut(P4_15, PIN_INPUT, PullNone, 0), .val = 16 + 32 * 4},
  {.pin = DigitalInOut(P4_16, PIN_INPUT, PullNone, 0), .val = 17 + 32 * 4},
  {.pin = DigitalInOut(P4_17, PIN_INPUT, PullNone, 0), .val = 18 + 32 * 4},
  {.pin = DigitalInOut(P4_18, PIN_INPUT, PullNone, 0), .val = 19 + 32 * 4},
  {.pin = DigitalInOut(P4_19, PIN_INPUT, PullNone, 0), .val = 20 + 32 * 4},
  {.pin = DigitalInOut(P4_20, PIN_INPUT, PullNone, 0), .val = 21 + 32 * 4},
  {.pin = DigitalInOut(P4_21, PIN_INPUT, PullNone, 0), .val = 22 + 32 * 4},
  {.pin = DigitalInOut(P4_22, PIN_INPUT, PullNone, 0), .val = 23 + 32 * 4},
  {.pin = DigitalInOut(P4_23, PIN_INPUT, PullNone, 0), .val = 24 + 32 * 4},
  {.pin = DigitalInOut(P4_24, PIN_INPUT, PullNone, 0), .val = 25 + 32 * 4},
  {.pin = DigitalInOut(P4_25, PIN_INPUT, PullNone, 0), .val = 26 + 32 * 4},
  {.pin = DigitalInOut(P4_26, PIN_INPUT, PullNone, 0), .val = 27 + 32 * 4},
  {.pin = DigitalInOut(P4_27, PIN_INPUT, PullNone, 0), .val = 28 + 32 * 4},
  {.pin = DigitalInOut(P4_28, PIN_INPUT, PullNone, 0), .val = 29 + 32 * 4},
  {.pin = DigitalInOut(P4_29, PIN_INPUT, PullNone, 0), .val = 30 + 32 * 4},
  {.pin = DigitalInOut(P4_30, PIN_INPUT, PullNone, 0), .val = 31 + 32 * 4},
  {.pin = DigitalInOut(P4_31, PIN_INPUT, PullNone, 0), .val = 32 + 32 * 4},
};

void output_pin(pins_t& pin)
{
  pin.pin.output();
  pin.pin = 1;
  wait_us(50);
  pin.pin = 0;
  wait_us(20);

  for(int8_t i = 0; i < 8; i++)
  {
    if((pin.val >> (7 - i)) & 1)
    {
      for(uint8_t j = 0; j < 15; j++)
      {
        pin.pin = 1;
      }
    }
    else
    {
      for(uint8_t j = 0; j < 3; j++)
      {
        pin.pin = 1;
      }
    }
    pin.pin = 0;
    wait_us(2);
  }

  pin.pin = 1;
  pin.pin.input();
}

void output_pin_pull(pins_t& pin)
{
  pin.pin.input();
  pin.pin.mode(PullUp);
  wait_us(5);
  pin.pin.mode(PullDown);
  wait_us(1000);

  for(int8_t i = 0; i < 8; i++)
  {
    if((pin.val >> (7 - i)) & 1)
    {
      pin.pin.mode(PullUp);
      wait_us(200);
    }
    else
    {
      pin.pin.mode(PullUp);
      wait_us(50);
    }
    pin.pin.mode(PullDown);
    wait_us(500);
  }

  pin.pin.mode(PullUp);
  pin.pin.input();
}

void toggleUnknownPins()
{
  for(auto& pin : unKnownPins)
  {
    output_pin(pin);
  }
}

#endif