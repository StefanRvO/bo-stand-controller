#include "ADCSampler.hpp"

ADCSampler::ADCSampler() : channels{A0, A1, A2, A3, A4, A5, P0_3, P0_2,},
    filters{EWMA(0.1), EWMA(0.1),EWMA(0.1),EWMA(0.1),EWMA(0.1),EWMA(0.1),EWMA(0.1),EWMA(0.1),}
{
}

void ADCSampler::update()
{
  for(uint8_t i = 0; i < channels.size(); i++)
  {
    filters[i].update(channels[i].read_u16());
  }
}