#include "common/Filters/EWMA.hpp"
#include "mbed.h"
#include <array>
#pragma once

class ADCSampler {
 public:
  ADCSampler();
  uint16_t getChannel(uint8_t channel) const { return filters[channel].output; }
  void     update();

 private:
  std::array<AnalogIn, 8> channels;
  std::array<EWMA, 8>     filters;
};