#include "debug.hpp"

static uint32_t debug_u32[4]   = {0};
static float    debug_float[4] = {0};

void set_debug_u32(uint8_t idx, uint32_t value)
{
  debug_u32[idx] = value;
}
void set_debug_float(uint8_t idx, float value)
{
  debug_float[idx] = value;
}
uint32_t get_debug_u32(uint8_t idx)
{
  return debug_u32[idx];
}
float get_debug_float(uint8_t idx)
{
  return debug_float[idx];
}
