#pragma once
#include <cstdint>
void     set_debug_u32(uint8_t idx, uint32_t value);
void     set_debug_float(uint8_t idx, float value);
uint32_t get_debug_u32(uint8_t idx);
float    get_debug_float(uint8_t idx);