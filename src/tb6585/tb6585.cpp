#include "tb6585.hpp"

namespace tb6585
{
tb6585::tb6585(PinName speed, PinName direction, PinName reset)
  : speedPin(speed),
    directionPin(direction),
    resetPin(reset)
{
  speedPin.period_us(10);
  resetPin = 0;
}

void tb6585::setSpeed(float speed)
{
  speed       = std::min(speed, 1.f);
  speed       = std::max(speed, -1.f);
  targetSpeed = speed;
}

void tb6585::update()
{
  updateSpeed();
}

void tb6585::updateSpeed()
{
  if(currentSpeed < targetSpeed)
  {
    currentSpeed += 0.01;
    currentSpeed = std::min(currentSpeed, targetSpeed);
  }
  else if(currentSpeed > targetSpeed)
  {
    currentSpeed -= 0.01;
    currentSpeed = std::max(currentSpeed, targetSpeed);
  }

  float speedPinValue = currentSpeed;

  if(speedPinValue < 0)
  {
    speedPinValue -= (0.5f / 3.3f);
  }
  else
  {
    speedPinValue += (0.5f / 3.3f);
  }

  speedPinValue = std::min(speedPinValue, 1.f);
  speedPinValue = std::max(speedPinValue, -1.f);

  if(fabsf(currentSpeed) < 0.02f)
  {
    speedPin = 0;
    if(get_ms_count() - lastSpeedTime > 1000 || lastSpeedTime == 0)
    {
      resetPin = 1;
    }
  }
  else
  {
    speedPin      = fabsf(speedPinValue);
    lastSpeedTime = get_ms_count();
    resetPin      = 0;
  }
  directionPin = signbit(currentSpeed);
}

}  // namespace tb6585