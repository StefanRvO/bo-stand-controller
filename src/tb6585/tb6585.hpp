#pragma once

// Driver for the TB6585 motor controller IC from Toshiba

#include <stdint.h>

#include "PulseCounter/PulseCounter.hpp"
#include "mbed.h"

namespace tb6585
{

class tb6585 {
 public:
  tb6585(PinName speed, PinName direction, PinName reset);
  ~tb6585() = default;

  void  setSpeed(float speed);
  void  update();  // Expects to be called at 500Hz
  float getCurrentSpeed() { return currentSpeed; }

 public:
  void updateSpeed();
  void updatePosition();

  float    currentSpeed  = 0;
  float    targetSpeed   = 0;
  uint64_t lastSpeedTime = 0;

  PwmOut     speedPin;
  DigitalOut directionPin;
  DigitalOut resetPin;
};
}  // namespace tb6585