#pragma once

#include "PulseCounter/PulseCounter.hpp"
#include "RingBuffer/RingBuffer.hpp"
#include "common/Filters/EWMA.hpp"
#include "common/Filters/MovingAverage.hpp"
#include "tb6585/tb6585.hpp"

class MotorFeedbackSpeed {
 public:
  MotorFeedbackSpeed(PulseCounter& counter, float gearing_to_encoder, tb6585::tb6585& motor)
    : counter(counter),
      slowSpeedFilter(0.001),
      gearing_to_encoder(gearing_to_encoder),
      motor(motor)
  {
    positionBuffer.fill(0);
  }
  uint32_t getCounter() { return counter.getCount(); }
  float    getSpeed()
  {
    float computedSpeed = speed.getLast() * 20 * gearing_to_encoder;
    if(motor.getCurrentSpeed() < 0)
    {
      computedSpeed *= -1;
    }
    return computedSpeed;
  }

  float getSlowFilteredSpeed() { return slowSpeedFilter.output; }
  void  update()
  {
    uint32_t count = counter.getCount();
    positionBuffer.add(count);
    samples++;
    if(samples < 51)
    {
      return;
    }
    speed.update(count - positionBuffer.at(-50));
    slowSpeedFilter.update(getSpeed());
  }

 private:
  PulseCounter&            counter;
  MovingAverage<float, 3>  speed;
  RingBuffer<uint32_t, 64> positionBuffer;
  EWMA                     slowSpeedFilter;
  float                    gearing_to_encoder;
  tb6585::tb6585&          motor;
  uint64_t                 samples = 0;
};