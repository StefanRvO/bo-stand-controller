#include <inttypes.h>

#include "ADCSampler/ADCSampler.hpp"
#include "I2CUtils/I2CUtils.hpp"
#include "LEDControl/LEDControl.hpp"
#include "MotorFeedbackSpeed/MotorFeedbackSpeed.hpp"
#include "PositionController/PositionController.hpp"
#include "PulseCounter/PulseCounter.hpp"
#include "SWO/SWO.h"
#include "SoftI2C/SoftI2C.h"
#include "as5046/as5046.hpp"
#include "bo_pins.h"
#include "common/Bootloader/Bootloader.hpp"
#include "common/CANMessages.hpp"
#include "common/CPULoadTimer/CPULoadTimer.hpp"
#include "common/DeviceStats/DeviceStats.hpp"
#include "common/EventTimer/EventTimer.hpp"
#include "common/TJA1055/TJA1055.hpp"
#include "common/setFrequency/setFrequency.hpp"
#include "debug/debug.hpp"
#include "mbed.h"
#include "tb6585/tb6585.hpp"

static const float                                  GEARING_M1 = 17.75f;
static const float                                  GEARING_M2 = 52.f;
__attribute__((section("AHBSRAM0"))) static uint8_t MOTOR_CONTROL_STACK[2048];

static as5046::as5046 encoder1(ENC1_SDA, ENC1_SCL, ENC1_CHIPSELECT, ENC1_PROG);
static as5046::as5046 encoder2(ENC2_SDA, ENC2_SCL, ENC2_CHIPSELECT, ENC2_PROG);
static CPULoadTimer   load_timer(LPC_TIM3);

static tb6585::tb6585     m1(M1_VSP, M1_DIRECTION, M1_RESET);
static tb6585::tb6585     m2(M2_VSP, M2_DIRECTION, M2_RESET);
static TJA1055            can(CAN_RX, CAN_TX, CAN_ERR_N, CAN_STB, CAN_EN);
static EventTimer         event_timer;
static PulseCounter       feedback_m1(LPC_TIM0, M1_FG, PulseCounter::CountEvent::RISING_FALLING, 0);
static MotorFeedbackSpeed feedback_speed_m1(feedback_m1, 1.f / GEARING_M1, m1);
static PulseCounter       feedback_m2(LPC_TIM1, M2_FG, PulseCounter::CountEvent::RISING_FALLING, 0);
static MotorFeedbackSpeed feedback_speed_m2(feedback_m2, 1.f / GEARING_M2, m2);

__attribute__((section("AHBSRAM0"))) static Trajectory<20>
  trajectory(encoder1, encoder2, load_timer, GEARING_M1, GEARING_M2);

static PositionController m1_controller(m1, encoder1, feedback_speed_m1);
static PositionController m2_controller(m2, encoder2, feedback_speed_m2);

static ADCSampler adc_sampler;

namespace mbed
{
FileHandle* mbed_override_console(int fd)
{
  static BufferedSerial console(P2_0, NC, 115200);
  return &console;
}
}  // namespace mbed

void publishEncoders()
{
  uint8_t data[4];
  *(reinterpret_cast<uint16_t*>(data + 0)) = encoder1.getPosition();
  *(reinterpret_cast<uint16_t*>(data + 2)) = encoder2.getPosition();

  can.write(
    mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_CONTROL_STATUS_ENCODERS), data, sizeof(data)));
}

void publishMotors()
{
  uint8_t data[8];
  *(reinterpret_cast<float*>(data + 0)) = m1.getCurrentSpeed();
  *(reinterpret_cast<float*>(data + 4)) = m2.getCurrentSpeed();

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_CONTROL_STATUS_MOTORS), data, sizeof(data)));
}

void publishEncoderSpeeds()
{
  uint8_t data[8];
  *(reinterpret_cast<float*>(data + 0)) = encoder1.getSpeed();
  *(reinterpret_cast<float*>(data + 4)) = encoder2.getSpeed();

  can.write(
    mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_CONTROL_STATUS_ENCODER_SPEED), data, sizeof(data)));
}

void publishLastNeededSpeeds()
{
  uint8_t data[8];
  *(reinterpret_cast<float*>(data + 0)) = m1_controller.getLastNeededSpeed();
  *(reinterpret_cast<float*>(data + 4)) = m2_controller.getLastNeededSpeed();

  can.write(
    mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_CONTROL_STATUS_NEEDED_SPEED), data, sizeof(data)));
}

void publishMotorFeedbackSpeeds()
{
  uint8_t data[8];
  *(reinterpret_cast<float*>(data + 0)) = feedback_speed_m1.getSpeed();
  *(reinterpret_cast<float*>(data + 4)) = feedback_speed_m2.getSpeed();

  can.write(mbed::CANMessage(
    static_cast<unsigned int>(CanMessageType::STAND_CONTROL_STATUS_MOTOR_FEEDBACK_SPEED),
    data,
    sizeof(data)));
}

void publishMotorFeedbackTicks()
{
  uint8_t data[8];
  *(reinterpret_cast<uint32_t*>(data + 0)) = feedback_speed_m1.getCounter();
  *(reinterpret_cast<uint32_t*>(data + 4)) = feedback_speed_m2.getCounter();

  can.write(mbed::CANMessage(
    static_cast<unsigned int>(CanMessageType::STAND_CONTROL_STATUS_MOTOR_FEEDBACK_TICKS),
    data,
    sizeof(data)));
}

void publishTargets()
{
  uint8_t data[8];
  *(reinterpret_cast<uint32_t*>(data + 0)) = m1_controller.getTarget();
  *(reinterpret_cast<uint32_t*>(data + 4)) = m2_controller.getTarget();

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_CONTROL_STATUS_TARGET), data, sizeof(data)));
}

void publishUint32Debug1()
{
  uint8_t data[8];
  *(reinterpret_cast<uint32_t*>(data + 0)) = get_debug_u32(0);
  *(reinterpret_cast<uint32_t*>(data + 4)) = get_debug_u32(1);

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_DEBUG_CHANNEL_UINT32_1), data, sizeof(data)));
}

void publishUint32Debug2()
{
  uint8_t data[8];
  *(reinterpret_cast<uint32_t*>(data + 0)) = get_debug_u32(2);
  *(reinterpret_cast<uint32_t*>(data + 4)) = get_debug_u32(3);

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_DEBUG_CHANNEL_UINT32_2), data, sizeof(data)));
}

void publishFloatDebug1()
{
  uint8_t data[8];
  *(reinterpret_cast<float*>(data + 0)) = get_debug_float(0);
  *(reinterpret_cast<float*>(data + 4)) = get_debug_float(1);

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_DEBUG_CHANNEL_FLOAT1), data, sizeof(data)));
}

void publishFloatDebug2()
{
  uint8_t data[8];
  *(reinterpret_cast<float*>(data + 0)) = get_debug_float(2);
  *(reinterpret_cast<float*>(data + 4)) = get_debug_float(3);

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_DEBUG_CHANNEL_FLOAT2), data, sizeof(data)));
}

void publishCRC()
{
  uint8_t data[4];
  *(reinterpret_cast<uint32_t*>(data + 0)) = __builtin_bswap32(Bootloader::get_crc_from_flash());

  can.write(mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_APPLICATION_CRC), data, sizeof(data)));
}

void publishTrajectoryStatus()
{
  uint8_t data[7];

  *(reinterpret_cast<uint32_t*>(data + 0)) = trajectory.getCRC();
  *(reinterpret_cast<uint8_t*>(data + 4))  = trajectory.getWaypointCount();
  *(reinterpret_cast<uint8_t*>(data + 5))  = trajectory.getCurrentM1Waypoint();
  *(reinterpret_cast<uint8_t*>(data + 6))  = trajectory.getCurrentM2Waypoint();

  can.write(
    mbed::CANMessage(static_cast<unsigned int>(CanMessageType::STAND_CONTROL_TRAJECTORY_STATUS), data, sizeof(data)));
}

void trajectoryWaypointCallback(CANMessage& msg)
{
  uint8_t waypoint_index = (*reinterpret_cast<uint16_t*>(msg.data + 0)) >> 12;
  waypoint_index         = waypoint_index << 4;
  waypoint_index += (*reinterpret_cast<uint16_t*>(msg.data + 2)) >> 12;

  trajectory.setWaypoint(
    {Waypoint(*reinterpret_cast<uint16_t*>(msg.data + 0) & 0xFFF),
     Waypoint(*reinterpret_cast<uint16_t*>(msg.data + 2) & 0xFFF)},
    waypoint_index);

  if(msg.len > 4)
  {
    waypoint_index = (*reinterpret_cast<uint16_t*>(msg.data + 4)) >> 12;
    waypoint_index = waypoint_index << 4;
    waypoint_index += (*reinterpret_cast<uint16_t*>(msg.data + 6)) >> 12;
    trajectory.setWaypoint(
      {Waypoint(*reinterpret_cast<uint16_t*>(msg.data + 4) & 0xFFF),
       Waypoint(*reinterpret_cast<uint16_t*>(msg.data + 6) & 0xFFF)},
      waypoint_index);
  }

  publishTrajectoryStatus();
}

void trajectoryStart(CANMessage& msg)
{
  uint32_t move_time    = *reinterpret_cast<uint32_t*>(msg.data + 0);
  uint8_t  waypoint_cnt = msg.data[4];
  uint32_t crc          = msg.data[5];
  crc                   = (crc << 8) + msg.data[6];
  crc                   = (crc << 8) + msg.data[7];

  if(crc == (trajectory.getCRC() & 0xFFFFFF))
  {
    trajectory.finalize(waypoint_cnt, move_time);
    m1_controller.setTrajectoryFunctions(
      std::bind(&Trajectory<20>::getNeededSpeedM1, &trajectory),
      std::bind(&Trajectory<20>::getTargetM1, &trajectory));

    m2_controller.setTrajectoryFunctions(
      std::bind(&Trajectory<20>::getNeededSpeedM2, &trajectory),
      std::bind(&Trajectory<20>::getTargetM2, &trajectory));
  }

  publishTrajectoryStatus();
}

void resetTrajectories([[maybe_unused]] CANMessage& msg)
{
  trajectory.reset();
  publishTrajectoryStatus();
}

bool isFeedbackSane(as5046::as5046& encoder, MotorFeedbackSpeed& feedback)
{
  if(std::abs(feedback.getSlowFilteredSpeed()) <= std::abs(encoder.getSlowFilteredSpeed()))
  {
    return true;
  }

  if(std::abs(encoder.getSlowFilteredSpeed()) / std::abs(feedback.getSlowFilteredSpeed()) > 0.8)
  {
    return true;
  }

  if(std::abs(std::abs(feedback.getSlowFilteredSpeed()) - std::abs(encoder.getSlowFilteredSpeed())) < 5)
  {
    return true;
  }
  return false;
}

void checkFeedbackSanity()
{
  static uint64_t last_sane = 0;
  if(!isFeedbackSane(encoder1, feedback_speed_m1) || !isFeedbackSane(encoder2, feedback_speed_m2))
  {
    if(get_ms_count() - last_sane > 2500)
    {
      m1_controller.stop();
      m2_controller.stop();
    }
  }
  else
  {
    last_sane = get_ms_count();
  }
}

void checkADCSanity()
{
  static uint64_t last_sane_fast = 0;
  static uint64_t last_sane_slow = 0;
  const uint64_t  currentTime    = get_ms_count();

  const float m1_current = adc_sampler.getChannel(2);
  const float m2_current = adc_sampler.getChannel(5);
  if(m1_current < 60000 && m2_current < 60000)
  {
    last_sane_fast = currentTime;
  }

  if(m1_current < 55000 && m2_current < 55000)
  {
    last_sane_slow = currentTime;
  }

  if(currentTime - last_sane_fast > 50 || currentTime - last_sane_slow > 500)
  {
    m1_controller.stop();
    m2_controller.stop();
  }
}

void motor_task()
{
  uint64_t nextWake = get_ms_count();

  while(1)
  {
    load_timer.startLoad();
    Watchdog::get_instance().kick();
    nextWake += 1;

    adc_sampler.update();
    set_debug_u32(0, ((uint32_t)adc_sampler.getChannel(0)) << 16 | adc_sampler.getChannel(1));
    set_debug_u32(1, ((uint32_t)adc_sampler.getChannel(2)) << 16 | adc_sampler.getChannel(3));
    set_debug_u32(2, ((uint32_t)adc_sampler.getChannel(4)) << 16 | adc_sampler.getChannel(5));
    set_debug_u32(3, ((uint32_t)adc_sampler.getChannel(6)) << 16 | adc_sampler.getChannel(7));

    encoder1.update();
    feedback_speed_m1.update();
    m1_controller.update();

    encoder2.update();
    feedback_speed_m2.update();
    m2_controller.update();

    checkFeedbackSanity();
    checkADCSanity();

    event_timer.step();
    can.recieveMessages();

    load_timer.endLoad();
    thread_sleep_until(nextWake);
  }
}

void resetCallback([[maybe_unused]] CANMessage& msg)
{
  NVIC_SystemReset();
}

__attribute__((used)) int main()
{
  setFrequency();

  const uint32_t WDOG_TIMEOUT_MS = 2500;
  Watchdog&      watchdog        = Watchdog::get_instance();
  watchdog.start(WDOG_TIMEOUT_MS);

  m1_controller.setTrajectoryFunctions(
    std::bind(&Trajectory<20>::getNeededSpeedM1, &trajectory),
    std::bind(&Trajectory<20>::getTargetM1, &trajectory));

  m2_controller.setTrajectoryFunctions(
    std::bind(&Trajectory<20>::getNeededSpeedM2, &trajectory),
    std::bind(&Trajectory<20>::getTargetM2, &trajectory));

  can.registerCallback(CanMessageType::STAND_CONTROL_REBOOT, resetCallback);
  can.registerCallback(CanMessageType::STAND_CONTROL_WAYPOINT, trajectoryWaypointCallback);
  can.registerCallback(CanMessageType::STAND_CONTROL_START_MOVEMENT, trajectoryStart);
  can.registerCallback(CanMessageType::STAND_CONTROL_STOP_MOVEMENT, resetTrajectories);

  event_timer.addEvent([]() { printf("APP RUNNING\n"); }, get_ms_count() + 0, 50);
  event_timer.addEvent(publishEncoders, get_ms_count() + 2, 50);
  event_timer.addEvent(publishMotors, get_ms_count() + 4, 50);
  event_timer.addEvent(publishCRC, get_ms_count() + 6, 100);
  event_timer.addEvent(publishTargets, get_ms_count() + 8, 50);
  event_timer.addEvent(publishEncoderSpeeds, get_ms_count() + 10, 50);

  event_timer
    .addEvent([]() { DeviceStats::sendHeap(can, CanMessageType::STAND_DEVICE_STATS_HEAP); }, get_ms_count() + 12, 500);
  event_timer
    .addEvent([]() { DeviceStats::sendUptime(can, CanMessageType::STAND_DEVICE_STATS_UPTIME); }, get_ms_count() + 14, 500);
  event_timer.addEvent(
    []() { DeviceStats::sendLoad(can, load_timer, CanMessageType::STAND_DEVICE_STATS_LOAD); },
    get_ms_count() + 16,
    500);

  event_timer.addEvent(publishLastNeededSpeeds, get_ms_count() + 18, 50);
  event_timer.addEvent(publishMotorFeedbackSpeeds, get_ms_count() + 20, 50);
  event_timer.addEvent(publishMotorFeedbackTicks, get_ms_count() + 22, 50);
  event_timer.addEvent(publishFloatDebug1, get_ms_count() + 24, 50);
  event_timer.addEvent(publishFloatDebug2, get_ms_count() + 26, 50);
  event_timer.addEvent(publishUint32Debug1, get_ms_count() + 28, 50);
  event_timer.addEvent(publishUint32Debug2, get_ms_count() + 30, 50);

  Thread* motor_thread =
    new Thread(osPriorityNormal, sizeof(MOTOR_CONTROL_STACK), MOTOR_CONTROL_STACK, "control_thread");
  motor_thread->start(motor_task);

  while(1)
  {
    thread_sleep_for(1000000);
  }

  return 0;
}
