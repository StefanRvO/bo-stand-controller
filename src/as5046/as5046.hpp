#pragma once
// Driver for the AS5046 magnetic encoder from Austria Microsystems

#include "RingBuffer/RingBuffer.hpp"
#include "RotaryPositionEstimator/RotaryPositionEstimator.hpp"
#include "SoftI2C/SoftI2C.h"
#include "common/CPULoadTimer/CPULoadTimer.hpp"
#include "common/Filters/EWMA.hpp"
#include "common/Filters/MovingAverage.hpp"
#include "mbed.h"
#include <complex>

namespace as5046
{
struct status {
  uint16_t position;
  uint8_t  status;
  uint8_t  fieldStrength;
  bool     valid;
};

#define RANGE_360_DEGREE 0
#define RANGE_180_DEGREE 1
#define RANGE_90_DEGREE  2
#define RANGE_45_DEGREE  3

class as5046 {
  typedef union {
    struct {
      uint16_t clockwise : 1;
      uint16_t zero_index : 10;
      uint16_t gain_internal : 1;
      uint16_t dac_external : 1;
      uint16_t analog_out_span : 1;
      uint16_t outputRange : 2;
    };
    uint16_t raw;
  } config_t;

 public:
  as5046(PinName sda, PinName scl, PinName chipSelect, PinName prog)
    : chipSelectPin(chipSelect, 0),
      progPin(prog, 1),
      i2cBus(sda, scl, 800000),
      positionBuffer(),
      slowSpeedFilter(0.001)
  {
    config_t _config;
    _config.clockwise       = 1;
    _config.zero_index      = 1;
    _config.gain_internal   = 1;
    _config.dac_external    = 1;
    _config.analog_out_span = 1;
    _config.outputRange     = RANGE_360_DEGREE;
    setConfig(_config);
  }

  ~as5046() = default;
  void           update();
  const status&  getStatus() const { return _status; }
  uint16_t       getPosition() const { return _status.position; }
  static int16_t getDistance(int16_t p1, int16_t p2);
  float          getSpeed() const { return speed.getLast(); }
  float          getSlowFilteredSpeed() { return slowSpeedFilter.output; }

 private:
  void                      setConfig(config_t _config);
  DigitalOut                chipSelectPin;
  DigitalOut                progPin;
  SoftI2C                   i2cBus;
  status                    _status = {0};
  MovingAverage<float, 4>   speed;
  RingBuffer<uint16_t, 128> positionBuffer;
  EWMA                      slowSpeedFilter;
  uint64_t                  samples = 0;
};
}  // namespace as5046