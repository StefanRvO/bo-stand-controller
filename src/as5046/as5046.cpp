#include "as5046.hpp"

#include <math.h>

namespace as5046
{
void as5046::update()
{
  uint8_t raw[4];
  i2cBus.read(80, reinterpret_cast<char*>(raw), sizeof(raw));

  if(raw[2] != 0xFF)
  {
    _status.position = static_cast<uint16_t>(raw[0]) << 4;
    _status.position += (raw[1] & 0xC0) >> 4;
    _status.position += raw[3] >> 6;

    _status.status        = raw[1] & 0x3F;
    _status.fieldStrength = raw[2];
    _status.valid         = true;
  }
  else
  {
    _status.valid = false;
    return;
  }
  samples++;
  positionBuffer.add(getPosition());
  if(samples < 126)
  {
    return;
  }
  speed.update(getDistance(positionBuffer.at(-125), getPosition()) * 8);
  slowSpeedFilter.update(speed.getLast());
}

int16_t as5046::getDistance(int16_t p1, int16_t p2)
{
  int16_t positionDiff = (p2 - p1) & 0xFFF;

  if(positionDiff >= (1 << 11))
  {
    positionDiff = -((1 << 12) - positionDiff);
  }
  return positionDiff;
}

void as5046::setConfig(config_t _config)
{
  i2cBus.getSCL() = 0;
  i2cBus.getSCL().output();
  progPin = 1;
  wait_us(10);
  chipSelectPin = 1;
  wait_us(10);

  for(uint8_t bit = 0; bit < 16; bit++)
  {
    // Set prog pin state
    progPin = _config.raw >> (15 - bit);
    wait_us(10);

    i2cBus.getSCL() = 1;
    wait_us(10);
    i2cBus.getSCL() = 0;
    wait_us(10);
  }
  progPin       = 1;
  chipSelectPin = 0;
}

}  // namespace as5046